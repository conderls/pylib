    ________ _______ _____  __________
    ___  __ )__  __ \__  / / /___  __/______ ______
    __  __  |_  / / /_  / / / __  /   ___/ /____/ /_
    _  /_/ / / /_/ / / /_/ /  _  /    /_  __//_  __/
    /_____/  \____/  \____/   /_/      /_/    /_/

BOUT++ pylib's documentation!
=================================

**Wellcome to BOUT++ pylib documentation**!

**BOUT++ pylib** is a ``Python2.x`` package intended to provide some common
tools required for performing Plasmas Fluid Simulations with
 [BOUT++](http://boutproject.github.io/index.html) framework.
The **BOUT++ pylib** in the latest release
[BOUT++ v4.0.0](https://github.com/boutproject/BOUT-dev/releases/tag/v4.0.0)
is ``Python3.x`` supported.

**Download** This repo is accessible on
[GitLab](https://gitlab.com/conderls/pylib)

**Bugs Report** The most bugs and issues are managed using the
[issue tracker](https://gitlab.com/conderls/pylib/issues).
All suggestions, comments and feature requests are gladly wellcome and
appreciated.

Getting Started
----------------

It's highly encouraged to use [Anaconda](https://www.continuum.io/downloads)
as the python environment.
Before starting to use the tools in this package, some envrionment variables
and required packages are suggested to be set up first.

```shell
$ git clone https://gitlab.com/conderls/pylib.git

# change the value by yourself
$ export BOUT_pylib=$(pwd)/pylib
$ export PATH=$BOUT_pylib/bin:$PATH
$ export PYTHONPATH=$BOUT_pylib:$PYTHONPATH

# install dependencies
$ pip install -r $BOUT_pylib/pip-requirements
# if the command above failed, try:
$ pip install --user -r $BOUT_pylib/pip-requirements
```

>:pushpin: **Warning**
- the commands above are **bash** commands which DO NOT work for
  **csh**, **zsh**, **ksh**. Type ``echo $SHELL`` in terminal to
  check which kind of shell you are using;
- for convience, one can put the ``line 4-6`` into ~/.bashrc with
  ``$(pwd)/pylib`` in ``line 4`` replaced by
  ``/your/path/to/this/pylib``;
- If ``line 9`` failed due to ``OSError(Permission denied)``,
  one needs to use 'user site' location ``line 11``.

User Manual
------------------
The document for this repo is available at
[ReadTheDocs](http://boutpy.readthedocs.io). The user manual for the latest
BOUT++ are available [here](http://bout-dev.readthedocs.io/en/latest/)
