# import boutdata as bd
# import boututils as bu
# import visualization as bv

# package version check
import numpy as np
import matplotlib as mpl
import pandas as pd
from distutils.version import StrictVersion

assert StrictVersion(np.__version__) > StrictVersion('1.13.1')
assert StrictVersion(mpl.__version__) > StrictVersion('2.0.2')
assert StrictVersion(pd.__version__) > StrictVersion('0.18.1')
