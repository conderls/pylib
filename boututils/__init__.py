##################################################
#            Data utilities package
#
# Generic routines, useful for all data
##################################################

from __future__ import (absolute_import, division,
                        print_function, unicode_literals)

# from showdata import showdata
# from plotdata import plotdata

from boututils.datafile import DataFile
from boututils.calculus import deriv, integrate
from boututils.linear_regression import linear_regression
from boututils.shell import shell, jobinfo
from boututils.ncpus import determineNumberOfCPUs
from boututils.launch import launch
from boututils.getmpirun import getmpirun
from boututils.fileio import save2nc, readsav, file_import, file_list
from boututils.pfile import pfile
from boututils.compare_inp import compare_inp, parser_config, multi_index
from boututils.elmsize import elmsize
