.. pylib documentation master file, created by
   sphinx-quickstart on Fri Jun  9 11:14:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

::

    ________ _______ _____  __________
    ___  __ )__  __ \__  / / /___  __/______ ______
    __  __  |_  / / /_  / / / __  /   ___/ /____/ /_
    _  /_/ / / /_/ / / /_/ /  _  /    /_  __//_  __/
    /_____/  \____/  \____/   /_/      /_/    /_/

BOUT++ pylib's documentation!
=================================

Version: |version|, Last updated: |today|

Wellcome to **BOUT++ pylib documentation**!

**BOUT++ pylib** is a ``Python2.x``
package intended to provide some common tools required for performing Plasmas
Fluid Simulations with `BOUT++`_ framework. The **BOUT++ pylib** in the latest
release `BOUT++ v4.0.0`_ is ``Python3.x`` supported.

**Download**
    This repo is accessible on `GitLab`_.
**Bugs Report**
    The most bugs and issues are managed using the `issue tracker`_.
    All suggestions, comments and feature requests are gladly wellcome and
    appreciated.

.. _GitLab: https://gitlab.com/conderls/pylib
.. _BOUT++: http://boutproject.github.io/index.html
.. _BOUT++ v4.0.0: https://github.com/boutproject/BOUT-dev/releases/tag/v4.0.0
.. _issue tracker: https://gitlab.com/conderls/pylib/issues

.. contents:: Table of Contents
   :local:

Getting Started
+++++++++++++++

It's highly encouraged to use `Anaconda`_ as the python environment.
Before starting to use the tools in this package, some envrionment variables
and required packages are suggested to be set up first.

.. code-block:: bash
    :linenos:
    :emphasize-lines: 4,11

    $ git clone https://gitlab.com/conderls/pylib.git

    # change the value by yourself
    $ export BOUT_pylib=$(pwd)/pylib
    $ export PATH=$BOUT_pylib/bin:$PATH
    $ export PYTHONPATH=$BOUT_pylib:$PYTHONPATH

    # install dependencies
    $ pip install -r $BOUT_pylib/pip-requirements
    # if the command above failed, try:
    $ pip install --user -r $BOUT_pylib/pip-requirements

.. warning::

    - the commands above are **bash** commands which DO NOT work for
      **csh**, **zsh**, **ksh**. Type ``echo $SHELL`` in terminal to
      check which kind of shell you are using;
    - for convience, one can put the ``line 4-6`` into ~/.bashrc with
      ``$(pwd)/pylib`` in ``line 4`` replaced by
      ``/your/path/to/this/pylib``;
    - If ``line 9`` failed due to ``OSError(Permission denied)``,
      one needs to use 'user site' location ``line 11``.

.. _Anaconda: https://www.continuum.io/downloads

User Documentation
++++++++++++++++++

The BOUT++ documents are available
`here <http://bout-dev.readthedocs.io/en/latest/>`_.
The user manual for this python package is on `ReadTheDocs
<http://boutpy.readthedocs.io>`_

.. toctree::
    :maxdepth: 3
    :name: bin

    bin/index
    boutdata/index
    boututils/index
    visualization/index

Code development
+++++++++++++++++++
It's strongly encouraged to follow the standard Python style conventions
as described here:

    * `Style Guide for C Code <http://python.org/dev/peps/pep-0007/>`_
    * `Style Guide for Python Code <http://python.org/dev/peps/pep-0008/>`_
    * `Docstring Conventions <http://python.org/dev/peps/pep-0257/>`_

You can use a code checker:

    * `pylint <http://www.logilab.org/857>`_
    * `pyflakes <https://pypi.python.org/pypi/pyflakes>`_
    * `pep8.py <http://svn.browsershots.org/trunk/devtools/pep8/pep8.py>`_
    * `autopep8 <https://pypi.python.org/pypi/autopep8>`_
    * `flake8 <https://pypi.python.org/pypi/flake8>`_
    * `vim-flake8 <https://github.com/nvie/vim-flake8>`_ plugin for
      automatically checking syntax and style with flake8

.. _PEP8: https://www.python.org/dev/peps/pep-0008/

In order to facilitate documentation using `Sphinx
<http://www.sphinx-doc.org/en/stable/>`_, the ``numpydoc`` extension is used
so that the docstrings will be handled correctly. The `User Manual
<https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt>`_ and
`an example <http://github.com/numpy/numpy/blob/master/doc/example.py>`_ of
``numpydoc`` conventions are available.

Useful python packages for Physics
++++++++++++++++++++++++++++++++++

* `astropy <http://www.astropy.org/>`_
* `spacepy <https://pythonhosted.org/SpacePy/>`_
* `sunpy <http://sunpy.org/>`_
* `eqtools <https://eqtools.readthedocs.io/en/latest/>`_
    Tools for interacting with magnetic equilibria

Indices and tables
++++++++++++++++++++++

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
