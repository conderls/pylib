.. _chapter-boututils:

================================
:mod:`boututils` -- useful tools
================================

.. contents:: Table of Contents
    :local:

:mod:`~boututils.calculus` -- Derivatives and Integrals
-------------------------------------------------------

.. autosummary::

    ~boututils.calculus.deriv
    ~boututils.calculus.integrate

.. automodule:: boututils.calculus
    :members:

:mod:`~boututils.compare_inp` -- parse & compare BOUT.inp
---------------------------------------------------------

.. autosummary::

    ~boututils.compare_inp.parser_config
    ~boututils.compare_inp.dict_to_level1
    ~boututils.compare_inp.compare_inp

.. automodule:: boututils.compare_inp
    :members:

:mod:`~boututils.datafile` -- Netcdf File I/O
---------------------------------------------
.. autoclass:: boututils.datafile.DataFile

:mod:`~boututils.fileio` -- data I/O methods
--------------------------------------------
.. autosummary::

    ~boututils.fileio.save2nc
    ~boututils.fileio.readsav
    ~boututils.fileio.file_import
    ~boututils.fileio.file_list

.. automodule:: boututils.fileio
    :members:

:mod:`~boututils.functions` -- useful functions
-----------------------------------------------

.. autosummary::

    ~boututils.functions.get_yesno
    ~boututils.functions.get_nth
    ~boututils.functions.get_limits
    ~boututils.functions.get_input
    ~boututils.functions.nearest
    ~boututils.functions.pycommand
    ~boututils.functions.nrange
    ~boututils.functions.sort_nicely

.. automodule:: boututils.functions
    :members:

:mod:`~boututils.pfile` -- Osborne pfiles I/O
---------------------------------------------

.. automodule:: boututils.pfile
    :members:

:mod:`~boututils.shell` -- Run shell command
--------------------------------------------

.. autosummary::

    ~boututils.shell.shell
    ~boututils.shell.jobinfo

.. automodule:: boututils.shell
    :members:
