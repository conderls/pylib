.. _chapter-visualization:

===========================================
:mod:`~visualization` -- data visualization
===========================================

.. contents:: Table of Contents
    :local:

:mod:`~visualization.plotconfig` -- General setup for plot
----------------------------------------------------------

.. autosummary::
    ~visualization.plotconfig.colors
    ~visualization.plotconfig.colors_default
    ~visualization.plotconfig.myplot_style
    ~visualization.plotconfig.color_list

.. automodule:: visualization.plotconfig
    :members:

:mod:`~visualization.plotfigs` -- plot figures
----------------------------------------------

.. autosummary::
    ~visualization.plotfigs.surface
    ~visualization.plotfigs.contourf
    ~visualization.plotfigs.savefig

.. automodule:: visualization.plotfigs
    :members:

