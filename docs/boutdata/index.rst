.. _chapter-boutdata:

=================================================
:mod:`boutdata` -- exchanging data to/from BOUT++
=================================================

.. contents:: Table of Contents
    :local:

:class:`~boutdata.boutgrid` -- More Information about BOUT++ Grid
-----------------------------------------------------------------

.. autoclass:: boutdata.boutgrid
    :members:

:class:`~boutdata.Field` -- Field Class
-----------------------------------------------------------------

.. autoclass:: boutdata.Field
    :members:
    :undoc-members:

:mod:`~boutdata.map_pfile2grid` -- Map Experimental Profile to Grid
-------------------------------------------------------------------

.. autosummary::

    ~boutdata.map_pfile2grid.map_1d4grid
    ~boutdata.map_pfile2grid.map_nc2grid
    ~boutdata.map_pfile2grid.map_pfile2grid

.. automodule:: boutdata.map_pfile2grid
    :members:

:mod:`~boutdata.collect` -- collect data
----------------------------------------
.. automodule:: boutdata.collect
    :members:

:mod:`~boutdata.restart` -- manipulate restart files
----------------------------------------------------
.. automodule:: boutdata.restart
    :members:

:mod:`~boutdata.gen_surface` -- Flux Surface Generator
------------------------------------------------------
.. automodule:: boutdata.gen_surface
    :members:

:mod:`~boutdata.pol_slice` -- Poloidal Slice
--------------------------------------------
.. automodule:: boutdata.pol_slice
    :members:
