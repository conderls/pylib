.. _chapter-bin:

====================================
**bin** -- executable python scripts
====================================

.. contents:: Table of Contents
    :local:

Introduction
------------
The python scripts in this directory are executable. They are useful for
running BOUT++ code and data processing.


Getting Started
---------------
To run the scripts directly, set the environment first.

.. code-block:: bash

    $ export PATH=/path/to/pylib/bin:$PATH
    $ chmod a+rx /path/to/pylib/bin/*   # change file mode to be executable
    $ compare_imp.py -h
    $ growthrate.py -h


.. _bin-compare_inp:

compare_inp.py -- Compare the BOUT.inp files
++++++++++++++++++++++++++++++++++++++++++++

.. program-output:: python bin/compare_inp.py -h
    :cwd: ../../

Examples:

.. code-block:: bash

    $ compare_inp.py */n10/BOUT.inp
    cases:
                         Name
    case0  diamag/n10
    case1  diamag_er_2Er/n10
    case2  diamag_er_Er/n10
    --------------------------------------------------
    differences:
                   case0 case1 case2
    Er_factor      1.0    2.0   1.0
    diamag_er      false  true  true
    diamag_phi0    false  true  true
    experiment_Er  false  true  true


.. seealso:: :mod:`~boututils.compare_inp`

growthrate.py -- calculate growth rate
++++++++++++++++++++++++++++++++++++++

.. program-output:: python bin/growthrate.py -h
    :cwd: ../../
    :returncode: 0

gamma_plot.py -- plot growthrate spectrum
+++++++++++++++++++++++++++++++++++++++++

.. program-output:: python bin/gamma_plot.py -h
    :cwd: ../../

jobsubmit.py -- submit serial jobs in NERSC
+++++++++++++++++++++++++++++++++++++++++++

.. program-output:: python bin/jobsubmit.py -h
    :cwd: ../../

jobstate.py -- check & update & delete jobs
+++++++++++++++++++++++++++++++++++++++++++

.. program-output:: python bin/jobstate.py -h
    :cwd: ../../

scan.py -- create parameter scan
++++++++++++++++++++++++++++++++

.. program-output:: python bin/scan.py -h
    :cwd: ../../
